function obj = addNoise(obj, rms)

% Convert to TIME-domain if needed
original_domain = obj.MultisineContainer.Domain;
if strcmp(original_domain,'FREQUENCY')
    obj.MultisineContainer.Domain = 'TIME';
end

dim = size(obj.MultisineContainer.Data{1});
obj.MultisineContainer = obj.MultisineContainer.run(@(x) x + rms * randn(dim));

if strcmp(original_domain, 'FREQUENCY')
    obj.MultisineContainer.Domain = 'FREQUENCY';
end

end
