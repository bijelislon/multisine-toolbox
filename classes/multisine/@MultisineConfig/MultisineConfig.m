classdef MultisineConfig < DataSettings
    % MULTISINECONFIG describes all the possible configuration parameters
    % needed to generate multi-dimensional multisine excitations.
    
    properties (Dependent) % CamelCase
        FrequencyMode           % LOWPASS or BANDPASS multisine
        AmplitudeMode           % Use either the power or bin amplitude settings
        GridMode                % See GRID_OPTIONS for all possible combinations
        
        GridSettings
        PowerSettings
        VoltageSettings
        
        DC
    end
    properties (Access = protected) %snake_case
        frequency_mode = 'LOWPASS'
        amplitude_mode = 'VOLTAGE'
        grid_mode = {'EVEN'}
        
        grid_settings = struct('bandwidth',1.4e6,'group_size',4,'grid_downsampling',1)
        power_settings = struct('average',{{10,0}},'peak',{{21,11}})
        voltage_settings = struct('bin_voltage',{{0,-50}})
        
        dc = 0
    end
    
    properties (Constant, Access = protected)
        FREQUENCY_MODE_OPTIONS = {'LOWPASS', 'BANDPASS'};
        AMPLITUDE_MODE_OPTIONS = {'POWER', 'VOLTAGE'};
        GRID_OPTIONS = {'FULL', 'ODD', 'EVEN', 'RANDOM'};
    end
    
    %% GETTERS AND SETTERS
    methods
        
        function obj = MultisineConfig(settingsInst)
            if nargin ~= 0
                if isa(settingsInst,'DataSettings')
                    obj = obj.copyDataSettings(settingsInst,obj);
                else
                    error('Supplied DataSettings was not of the correct class.')
                end
            end
        end
        
        function frequency_mode = get.FrequencyMode(obj)
            frequency_mode = obj.frequency_mode;
        end
        
        function obj = set.FrequencyMode(obj, frequency_mode)
            if any(strcmpi(frequency_mode,MultisineConfig.FREQUENCY_MODE_OPTIONS))
                obj.frequency_mode = frequency_mode;
            end
        end
        
        function amplitude_mode = get.AmplitudeMode(obj)
            amplitude_mode = obj.amplitude_mode;
        end
        
        function obj = set.AmplitudeMode(obj, amplitude_mode)
            if any(strcmpi(amplitude_mode,MultisineConfig.AMPLITUDE_MODE_OPTIONS))
                obj.amplitude_mode = amplitude_mode;
            end
        end
        
        function grid = get.GridMode(obj)
            grid = obj.grid_mode;
        end
        
        function obj = set.GridMode(obj, grid)
            if MultisineConfig.check_GridMode(grid)
                obj.grid_mode = MultisineConfig.match_GridMode(grid);
            end
        end
        
        function grid_settings = get.GridSettings(obj)
            grid_settings = obj.grid_settings;
        end
        
        function obj = set.GridSettings(obj, grid_settings)
            if MultisineConfig.check_GridSettings(grid_settings)
                obj.grid_settings = grid_settings;
            end
        end
        
        function power_settings = get.PowerSettings(obj)
            power_settings = obj.power_settings;
        end
        
        function obj = set.PowerSettings(obj, power_settings)
            if MultisineConfig.check_PowerSettings(power_settings)
                obj.power_settings = power_settings;
            end
        end
        
        function voltage_settings = get.VoltageSettings(obj)
            voltage_settings = obj.voltage_settings;
        end
        
        function obj = set.VoltageSettings(obj, voltage_settings)
            if MultisineConfig.check_VoltageSettings(voltage_settings)
                fields = fieldnames(voltage_settings);
                for i_field=1:length(fields)
                    if isnumeric(voltage_settings.(fields{i_field}))
                        voltage_settings.(fields{i_field}) = {voltage_settings.(fields{i_field})};
                    end
                end
                obj.voltage_settings = voltage_settings;
            end
        end
        
        function dc = get.DC(obj)
            dc = obj.dc;
        end
        
        function obj = set.DC(obj, dc)
            if isscalar(dc)
                obj.dc = dc;
            end
        end
    end
    
    %% CHECKERS
    methods (Static, Access = protected)
        
        function res = check_GridSettings(val)
            check={'bandwidth' @isscalar;'group_size',@isscalar;'grid_downsampling',@isscalar};
            res = MultisineConfig.checkSettings(val,check,'GridSettings');
        end
        
        function res = check_PowerSettings(val)
            check={'average' @iscell;'peak' @iscell};
            res = MultisineConfig.checkSettings(val,check,'PowerSettings');
        end
        
        function res = check_VoltageSettings(val)
            check={'bin_voltage' @(x) iscell(x) || isnumeric(x)};
            res = MultisineConfig.checkSettings(val,check,'VoltageSettings');
        end
        
        function res = checkSettings(val,check,name)
            if isstruct(val)
                for ii=1:size(check,1)
                    if isfield(val,check{ii,1})
                        if ~check{ii,2}(val.(check{ii,1}))
                            error([name '.' check{ii,1} ' is not right']);
                        end
                    else
                        error([name ' needs the field: ' check{ii,1}])
                    end
                end
                res = true;
            else
                error([name ' should be a struct']);
            end
        end
        
        function res = check_GridMode(grid_mode)
            % Regular expression expansion
            words = regexp(grid_mode, '-+', 'split');
            try
                cellfun(@(x) validatestring(x, MultisineConfig.GRID_OPTIONS), words, 'uni', false);
            catch
                error('The provided Grid was not a valid possibility in MultisineConfig.GRID_OPTIONS.')
            end
            res = true;
        end
        
        function match = match_GridMode(grid_mode)
                words = regexp(grid_mode, '-+', 'split');
                match = cellfun(@(x) validatestring(x, MultisineConfig.GRID_OPTIONS), words, 'uni', false);
        end
        
    end
end  
