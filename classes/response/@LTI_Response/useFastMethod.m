function obj = useFastMethod(obj, local_model_config)
% Use LPM on the excited frequency lines to derive an estimate of the FRF
% matrix together with the covariance matrices

% Local variables
P  = floor(obj.NumberOfPeriods);
Ny = obj.NumberOfOutputs;
Nu = obj.NumberOfInputs;

% Define whether the input and output should be stacked or not
data_settings = obj.getDataSettings(obj);
if ~isempty(obj.reference)
    R = obj.reference;
    
    % Stack the output and input
    Z = LTI_Response.stackZ(obj.output, obj.input);
    data_settings.NumberOfOutputs = data_settings.NumberOfInputs + data_settings.NumberOfOutputs;
else
    R = obj.input;
    Z = obj.output;
end

%%  Noise transient removal if more than 1 period

if (local_model_config.LocalTransientOrder > 0) && (P > 1)
    % Concatenate the different periods
    R.Domain = 'TIME';
    Z.Domain = 'TIME';
    R_all_periods = R.concatenatePeriods;
    Z_all_periods = Z.concatenatePeriods;
    R_all_periods.Domain = 'FREQUENCY';
    Z_all_periods.Domain = 'FREQUENCY';
    R.Domain = 'FREQUENCY';
    Z.Domain = 'FREQUENCY';

    data_settings.NumberOfPeriods = 1;
    data_settings.FrequencySettings.fres = data_settings.FrequencySettings.fres / P;
    
    % Initialize the excited and adjacent bins
    local_transient_config = local_model_config;
    local_transient_config.ExcitedBins = (local_model_config.ExcitedBins{1}-1) * P + 1;
    start_adjacent = local_transient_config.ExcitedBins{1}(1) - (P - 1);
    stop_adjacent = local_transient_config.ExcitedBins{1}(end) + P - 1;
    adjacent_bins = start_adjacent:stop_adjacent;
    adjacent_bins(P:P:end) = [];
    local_transient_config.AdjacentBins = adjacent_bins;
    local_transient_config.LocalModelOrder = [];
    local_transient_config.LocalWindowWidth = local_transient_config.LocalWindowWidth(1);

    % Perform the local modelling
    lm = LocalModelling(data_settings, local_transient_config, 'out', Z_all_periods, 'in', R_all_periods);
    lm = lm.run();
    
    % Extract important features
    data_settings.FrequencySettings.fres = data_settings.FrequencySettings.fres * P;
    
    % Subtract the transient from Z at the excited frequencies
    Z = lm.Z - lm.T;
    
    % Extract input for next stage
    R = lm.R;
    
    % Derive the covariance matrix of the sample mean over the periods
    CZ = lm.CZNoise + lm.CT;
    
    % Make estimates visible to the outside world
    obj.CovarianceZ.Noise = CZ;
    obj.Z.Periods = Z;
    
    % Assign correct values for the remaining modelling step
    local_model_config.LocalTransientOrder = [];
    local_model_config.OutputNoiseVariance = CZ;
    local_model_config.ExcitedBins{1} = 1:numel(local_model_config.ExcitedBins{1});
    
    % Set the model interpolation for the transient
    obj.ModelInterpolationT = lm.ModelInterpolationT;
    obj.ModelInterpolationT.GridValues = {R.DomainValues};
else
    obj.CovarianceZ.Noise = [];
end

%% Apply the LPM to derive the FRF

% Perform the identification on the excited lines
lm = LocalModelling(data_settings, local_model_config, 'out', Z, 'in', R);
lm = lm.run();

% Split G into its two contributions
if ~isempty(obj.reference)
    [Gry, Gru] = obj.splitZ(lm.G, obj.NumberOfOutputs);
    obj.BLA = Gry / Gru;
    CvecBLA = obj.BLA.run(@(x, y, z) obj.calculateCovarianceMatrix(x, y, z), lm.G, lm.CvecG);
    obj.CovarianceBLA.Total = CvecBLA.run(@(x) obj.selectDiagonalCovarianceMatrix(x, Ny, Nu, true));
    if P > 1
        CvecBLAnoise = obj.BLA.run(@(x, y, z) obj.calculateCovarianceMatrix(x, y, z), lm.G, lm.CvecGNoise);
        obj.CovarianceBLA.Noise = CvecBLAnoise.run(@(x) obj.selectDiagonalCovarianceMatrix(x, Ny, Nu, true));
        obj.CovarianceBLA.Stochastic = LTI_Response.deriveStochasticDistortions(obj.CovarianceBLA.Total, obj.CovarianceBLA.Noise);
    end
else
    obj.BLA = lm.G;
    obj.CovarianceBLA.Total = lm.CvecG.run(@(x) obj.selectDiagonalCovarianceMatrix(x, Ny, Nu, true));
    if P > 1
        obj.CovarianceBLA.Noise = lm.CvecGNoise.run(@(x) obj.selectDiagonalCovarianceMatrix(x, Ny, Nu, true));
        obj.CovarianceBLA.Stochastic = LTI_Response.deriveStochasticDistortions(obj.CovarianceBLA.Total, obj.CovarianceBLA.Noise);
    end
end

obj.Z.Mean = lm.Z;
obj.CovarianceZ.TotalMean = lm.CZMean;
obj.CovarianceZ.Total = lm.CZNoise;

[obj.Y.Mean, obj.U.Mean] = LTI_Response.splitZ(obj.Z.Mean, Ny);
[obj.CovarianceY.Total, obj.CovarianceU.Total] = LTI_Response.splitCovarianceZ(obj.CovarianceZ.Total, Ny, Nu);
if ~isempty(obj.CovarianceZ.Noise)
    [obj.CovarianceY.Noise, obj.CovarianceU.Noise] = LTI_Response.splitCovarianceZ(obj.CovarianceZ.Noise, Ny, Nu);
    obj.CovarianceZ.Stochastic = LTI_Response.deriveStochasticDistortions(obj.CovarianceZ.Total, obj.CovarianceZ.Noise);
    obj.CovarianceY.Stochastic = obj.CovarianceZ.Stochastic.run(@(x,y) LTI_Response.deriveOutputStochasticDistortions(x, y, Ny), obj.BLA);
end

% Save the model coefficients for later interpolation use
obj.ModelInterpolationG = lm.ModelInterpolationG;
obj.ModelInterpolationG.GridValues = [{R.DomainValues(:)}, lm.ExternalVariables];
end