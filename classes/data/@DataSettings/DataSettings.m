classdef DataSettings
    % DATASETTINGS contains all the settings which are common to both the
    % simulation and/or measurement experiments.
    
    properties (Dependent) % CamelCase
        FrequencySettings
        
        NumberOfInputs          % Number of input ports
        NumberOfOutputs         % Number of output ports
        NumberOfExperiments     % Number of Experiments
        
        NumberOfRealisations    % Number of realisations
        NumberOfPeriods         % Number of periods
    end
    
    properties (Access = protected) %snake_case
        frequency_settings = struct('fs', 4e9, 'fres', 25e3);
        
        number_of_inputs = 2
        number_of_outputs = 3
        number_of_experiments = 2
        
        number_of_realisations = 7
        number_of_periods = 2
    end
    
    properties (Hidden = true)
        default_experiments = true          % Should the number of experiments be derived from the number of inputs?.
    end
    
    %% OTHER METHODS
    methods
        function derived_settings = getDerivedSettings(obj)
            % Derive settings from the frequency settings.
            derived_settings.N = round(obj.frequency_settings.fs/obj.frequency_settings.fres);
            derived_settings.tstep = 1/obj.frequency_settings.fs;
            derived_settings.tstop = ...
                (derived_settings.N*obj.number_of_periods-1)*derived_settings.tstep;
            derived_settings.numSamples = ...
                derived_settings.N*obj.number_of_periods;
            derived_settings.numericStop = ...
                derived_settings.numSamples-1;
        end
    end
    
    %% GETTERS & SETTERS
    methods
        function frequency_settings = get.FrequencySettings(obj)
            frequency_settings = obj.frequency_settings;
        end
        
        function obj = set.FrequencySettings(obj,frequency_settings)
            if DataSettings.check_FrequencySettings(frequency_settings)
                obj.frequency_settings = frequency_settings;
            end
        end
        
        function number_of_inputs = get.NumberOfInputs(obj)
            number_of_inputs = obj.number_of_inputs;
        end
        
        function obj = set.NumberOfInputs(obj, number_of_inputs)
            if isscalar(number_of_inputs) && isnumeric(number_of_inputs)
                obj.number_of_inputs = number_of_inputs;
                if obj.default_experiments
                    obj.number_of_experiments = number_of_inputs;
                end
            end
        end
        
        function number_of_outputs = get.NumberOfOutputs(obj)
            number_of_outputs = obj.number_of_outputs;
        end
        
        function obj = set.NumberOfOutputs(obj, number_of_outputs)
            if isscalar(number_of_outputs) && isnumeric(number_of_outputs)
                obj.number_of_outputs = number_of_outputs;
            end
        end
        
        function number_of_experiments = get.NumberOfExperiments(obj)
            number_of_experiments = obj.number_of_experiments;
        end
        
        function obj = set.NumberOfExperiments(obj, number_of_experiments)
            if isscalar(number_of_experiments) && isnumeric(number_of_experiments)
                obj.number_of_experiments = number_of_experiments;
                obj.default_experiments = false;
            end
        end
        
        function number_of_realisations = get.NumberOfRealisations(obj)
            number_of_realisations = obj.number_of_realisations;
        end
        
        function obj = set.NumberOfRealisations(obj, number_of_realisations)
            if isscalar(number_of_realisations) && isnumeric(number_of_realisations)
                obj.number_of_realisations = number_of_realisations;
            end
        end
        
        function number_of_periods = get.NumberOfPeriods(obj)
            number_of_periods = obj.number_of_periods;
        end
        
        function obj = set.NumberOfPeriods(obj, number_of_periods)
            if isscalar(number_of_periods) && isnumeric(number_of_periods)
                obj.number_of_periods = number_of_periods;
            end
        end
       
    end
    
    %% CHECKERS
    methods(Static, Access = protected)
        function res = check_FrequencySettings(val)
            check={'fs' @isscalar;'fres' @isscalar};
            res = DataSettings.check_Settings(val,check,'FrequencySettings');
        end
        
        function res = check_Settings(val,check,name)
            if isstruct(val)
                for ii=1:size(check,1)
                    if isfield(val,check{ii,1})
                        if ~check{ii,2}(val.(check{ii,1}))
                            error([name '.' check{ii,1} ' is not right']);
                        end
                    else
                        error([name ' needs the field: ' check{ii,1}])
                    end
                end
                res = true;
            else
                error([name ' should be a struct']);
            end
        end
    end
    
    %% STATIC METHODS
    methods(Static, Access = protected)
        
        function output = copyDataSettings(input, output)
            C = metaclass(input);
            P = C.Properties;
            for k = 1:length(P)
                if ~P{k}.Dependent && ~P{k}.Constant && strcmp(P{k}.DefiningClass.Name,'DataSettings')
                    output.(P{k}.Name) = input.(P{k}.Name);
                end
            end
        end
        
        function output = getDataSettings(input)
            C = metaclass(input);
            P = C.Properties;
            output = DataSettings();
            for k = 1:length(P)
                if ~P{k}.Dependent && ~P{k}.Constant && strcmp(P{k}.DefiningClass.Name,'DataSettings')
                    output.(P{k}.Name) = input.(P{k}.Name);
                end
            end
        end
        
    end
    
end


