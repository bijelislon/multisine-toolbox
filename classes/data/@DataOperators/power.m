function obj = power(obj, scalar)
% Take the POWER of a DataContainer with a scalar.

if isa(obj, 'DataContainer')
    if isscalar(scalar) && isnumeric(scalar)
        % Check the size of the matrix
        obj.Data = cellfun(@(x) x.^scalar, obj.Data, 'uni', false);
    else
        error('Type of scalar was invalid.')
    end
elseif isa(obj, 'MultisineSignal')
    obj.MultisineContainer = feval(mfilename,obj.MultisineContainer,scalar);
else
    error([mfilename ' functionality was not yet implemented for this class.']);
end
end