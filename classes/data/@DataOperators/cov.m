function obj = cov(obj,args)
% Take the COV of a DataContainer over either Periods or Realisations.

validStrings = {'Realisations','Periods'};

obj = vec(obj);
if isa(obj,'DataContainer')
    validString = validatestring(args,validStrings);
    switch validString
        case validStrings{1} %Realisations
            var_dim = @(k) covx(cat(3,obj.Data{:,k}));
            X = size(obj.Data,2);
        case validStrings{2} %Periods
            var_dim = @(k) covx(cat(3,obj.Data{k,:}));
            X = size(obj.Data,1);
        otherwise
            error('Input string was not recognized!')
    end
    
    obj.Data = arrayfun(var_dim ,1:X,'uni',false);
    obj.Data = obj.Data.';
else
    error([mfilename ' functionality was not yet implemented for this class.']);
end

end

function B = covx(A)
% Get the covariance matrix at each frequency bin.

[NzNe,F,~] = size(A);
B = zeros(NzNe,NzNe,F);
for i_freq = 1:F
    B(:,:,i_freq) = cov(squeeze(A(:,i_freq,:)).');
end

end

function obj = vec(obj)
% Stack the columns on top of each other.

N = length(obj);
obj.Data = cellfun(@(x) reshape(x,[numel(x)/N N]),obj.Data, 'uni', false);

end