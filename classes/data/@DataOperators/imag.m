function obj = imag(obj)
% Take the IMAGINARY part of the DataContainer object.

if isa(obj,'DataContainer')
    obj = obj.run(@imag);
elseif isa(obj,'MultisineSignal')
    obj.MultisineContainer = feval(mfilename,obj.MultisineContainer);
else
    error([mfilename ' functionality was not yet implemented for this class.']);
end

end
