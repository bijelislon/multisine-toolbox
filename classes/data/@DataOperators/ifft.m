function obj = ifft(obj)
% Take the IFFT of a DataContainer, MultisineSignal or
% MultisineResponse object.

if isa(obj,'DataContainer')
    if strcmp(obj.Domain,'FREQUENCY')
        % Setter does the fft.
        obj.Domain = 'TIME';
    else
        warning('DataContainer was already in the time domain, ignoring call.');
    end
elseif isa(obj,'MultisineSignal')
    obj.MultisineContainer = feval(mfilename,obj.MultisineContainer);
else
    error([mfilename ' functionality was not yet implemented for this class.']);
end

end

