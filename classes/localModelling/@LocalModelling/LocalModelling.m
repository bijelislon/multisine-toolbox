classdef LocalModelling < LocalModelConfig & DataSettings
    % LocalModelling uses the Reference, Input and Output MultisineSignal,
    % combined with nD-LPM to obtain the Best Linear Approximation
    %
    % SIGNAL Properties:
    %    Reference      - Reference signal (MULTISINESIGNAL)
    %    Input          - Input signal (MULTISINESIGNAL)
    %    Output         - Output signal (MULTISINESIGNAL)
    %
    % Methods:
    %    Localmodelling     - Class constructer that creates a MSResponse object
    %
    
    properties (Dependent) %CamelCase
        Input
        Output
    end
    
    properties (Access = protected) %snake_case
        input
        output
    end
    
    % Internal variables which contain the results of the local modelling
    properties (SetAccess = private, GetAccess = public)
        R
        Z
        CZNoise
        CZMean
        G
        CvecG
        CvecGNoise
        T
        CT
        ModelCoefficients
        ModelInterpolationG
        ModelInterpolationT
        ScalingGrid
    end
    
    % Constants
    properties (Constant, Access=protected)
        METHOD_LIST = {'FAST','ROBUST'}
    end
    
    %% CONSTRUCTOR
    methods
        
        function obj = LocalModelling(settingsInst, localModelConfig, varargin)
            if  nargin ~= 0
                p = inputParser;
                p.StructExpand = true;
                p.FunctionName = 'LocalModelling';
                p.PartialMatching = true;
                p.addRequired('settingsInst'       , @(x) isa(x,'DataSettings'));
                p.addRequired('localModelConfig'   , @(x) isa(x,'LocalModelConfig'));
                p.addParameter('input'          ,[], @(x) isa(x,'DataContainer') || isa(x,'MultisineSignal') || iscell(x));
                p.addParameter('output'         ,[], @(x) isa(x,'DataContainer') || isa(x,'MultisineSignal') || iscell(x)); 
                p.parse(settingsInst, localModelConfig, varargin{:});
                args = p.Results;
                
                % Copy DataSettings and LocalModelConfig
                obj = obj.copyDataSettings(args.settingsInst, obj);
                obj = obj.copyLocalModelConfig(args.localModelConfig, obj);
                
                % Assign variables (check the data consistency with the multisine_signal!)
                obj.Input = args.input;
                obj.Output = args.output;
            end
        end
    end
    
    %% OTHER METHODS
    
    methods (Access = protected)
        
        function [dof, parameters_model, parameters_transient] = calculateDOF(obj)
            
            number_local_model = 0;
            number_local_transient = 0;
            number_local_parameters = 0;
            
            % Derive number of local parameters for each basis
            if ~isempty(obj.local_model_order)
                number_local_model = (obj.local_model_order + 1) * obj.number_of_inputs;
                number_local_parameters = number_local_model;
            end
            if ~isempty(obj.local_transient_order)
                number_local_transient = obj.local_transient_order + 1;
                number_local_parameters = number_local_parameters + number_local_transient;
            end
            
            % Derive the total number of points for each basis
            number_local_points = 2*obj.local_window_width + 1;
            if ~isempty(obj.adjacent_bins)
                number_local_points = number_local_points - 1;
            end
            if any((number_local_points - (number_local_parameters)) < 0)
                warning('Too small window for the number of parameters.');
            end
            
            % Derive total number of parameters and degrees of freedom
            parameters_model = prod(number_local_model);
            parameters_transient = prod(number_local_transient);
            number_parameters = parameters_model + parameters_transient;
            number_points = prod(number_local_points);
            dof = number_points - number_parameters;
            if dof < 0
                error('Too many parameters for the number of local window points.');
            end            
        end
    end
    
    methods (Static, Access = protected)
        
       function local_index = extractLocalSpace(basis, index, window_width, adjacent) 
            
            basis_length = numel(basis);
            
            if adjacent
                if index < window_width
                    local_index = -index+1:2*window_width-index;
                elseif index >= basis_length-window_width+1
                    local_index = -2*window_width+basis_length-index+1:basis_length-index;
                else
                    local_index = -window_width+1:window_width;
                end
            else
                if index <= window_width
                    local_index = -index+1:2*window_width-index+1;
                elseif index >= basis_length-window_width+1
                    local_index = -2*window_width+basis_length-index:basis_length-index;
                else
                    local_index = -window_width:window_width;
                end
            end
       end
       
       function signal = implodeCellToMatrix(basis, signal)
            if numel(basis) > 1
                collapse_dimension = numel(basis) - 1;
                while collapse_dimension > 0

                    internal_size = size(signal{1});
                    external_size = size(signal);

                    % Decrement external size by one
                    aux_cell = cell(external_size(1:end-1));

                    % Augment internal size by one
                    aux_cell = cellfun(@(x) zeros([internal_size, external_size(end)]), aux_cell, 'uni', false);
                    
                    % Other dimensions of signal
                    dims_signal = repmat({':'}, 1, ndims(signal)-1);

                    % Convert cell dimension to matrix dimension
                    for i_cell = 1:external_size(end)
                        aux_cell = cellfun(@(x,y) LocalModelling.implodeDimension(x,y,i_cell), signal(dims_signal{:}, i_cell), aux_cell, 'uni', false);
                    end
                    
                    % Shift the internal dimensions
                    signal = aux_cell;
                    
                    collapse_dimension = collapse_dimension - 1;
                end
            end
       end
        
       function y = implodeDimension(x,y,index)
            dims_x = repmat({':'}, 1, ndims(x));
            dims_y = repmat({':'}, 1, ndims(y)-1);
            
            y(dims_y{:}, index) = x(dims_x{:});
        end
        
    end
    
    %% GETTERS AND SETTERS
    methods
        
        % GET and SET Input
        function input = get.Input(obj)
            input = obj.input;
        end
        
        function obj = set.Input(obj, input)
            if ~(isa(input,'DataContainer') || isa(input,'MultisineSignal') || iscell(input))
                error('Input data was not of the correct type.');
            end
            obj.input = LocalModelling.modifySignal(obj,input);
        end
        
        % GET and SET Output
        function output = get.Output(obj)
            output = obj.output;
        end
        
        function obj = set.Output(obj, output)
            if ~(isa(output,'DataContainer') || isa(output,'MultisineSignal') || iscell(output))
                error('Output data was not of the correct type.');
            end
            obj.output = LocalModelling.modifySignal(obj,output);
        end    
    end
    
    %% CHECKERS
    methods (Static , Access = protected)
        
        function data_container = modifySignal(obj, signal)
            if ~isempty(signal)
                switch class(signal)
                    case 'DataContainer'
                        data_container = signal;
                        
                    case 'MultisineSignal'
                        data_container = signal.MultisineContainer;

                    case 'cell'
                        data_container = DataContainer(signal,obj.getDataSettings(obj),'split_periods', true);
                end
                % Convert the signal to the FREQUENCY domain
                data_container.Domain = 'FREQUENCY';
            else
                data_container = [];
            end
        end
        
        function x = setDataBins(x,y,bins)
            x(:,:,bins) = y;
        end
        
    end
end