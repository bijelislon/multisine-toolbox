
clear
close all
clc

%% GENERAL SETTINGS

numberOfRealisations = 2;
fres = 0.25e6; % frequency resolution
fs = 4e9; % sampling frequency
P = 3.2; % Number of periods
averagePower = 10;

tickler_bandwidth = 60e6;
ms_bandwidth = 10e6;

%% GENERATE ALL SETTINGS.

settingsInst = DataSettings();
settingsInst.FrequencySettings.fres = fres;
settingsInst.FrequencySettings.fs = fs;
settingsInst.NumberOfPeriods = P;
settingsInst.NumberOfRealisations = numberOfRealisations;

%% SIGNAL GENERATION.

msConfigInst_supplyTickler = MultisineConfig(settingsInst);
msConfigInst_supplyTickler.NumberOfInputs = 1;
msConfigInst_supplyTickler.GridMode = 'ODD-ODD';
msConfigInst_supplyTickler.AmplitudeMode = 'VOLTAGE';
msConfigInst_supplyTickler.GridSettings.grid_downsampling = 1;
msConfigInst_supplyTickler.GridSettings.bandwidth = tickler_bandwidth;
msConfigInst_supplyTickler.VoltageSettings.bin_voltage = -50;

ms_supplyTickler = msConfigInst_supplyTickler.generateMultisine;
exportedData_supplyTickler = ms_supplyTickler.exportData;

msConfigInst_main = MultisineConfig(settingsInst);
msConfigInst_main.NumberOfInputs = 1;
msConfigInst_main.GridMode = 'EVEN-EVEN';
msConfigInst_main.FrequencyMode = 'BANDPASS';
msConfigInst_main.AmplitudeMode = 'POWER';
msConfigInst_main.GridSettings.bandwidth = ms_bandwidth;
msConfigInst_main.PowerSettings.average = {averagePower};
msConfigInst_main.PowerSettings.peak = {21};

ms_main = msConfigInst_main.generateMultisine();

msConfigInst_inputTickler = MultisineConfig(settingsInst);
msConfigInst_inputTickler.NumberOfInputs = 1;
msConfigInst_inputTickler.GridMode = 'ODD-EVEN';
msConfigInst_inputTickler.FrequencyMode = 'BANDPASS';
msConfigInst_inputTickler.AmplitudeMode = 'VOLTAGE';
msConfigInst_inputTickler.GridSettings.grid_downsampling = 4;
msConfigInst_inputTickler.GridSettings.bandwidth = tickler_bandwidth;
msConfigInst_inputTickler.VoltageSettings.bin_voltage = -100;

ms_inputTickler = msConfigInst_inputTickler.generateMultisine;

ms_total = ms_inputTickler + ms_main;

%% PLOT SOME VALIDATION FIGURES.
        
plot(ms_supplyTickler,'Marker','x','Color','r')
title('Supply tickler')

plot(ms_main,'Marker','o')
hold on
plot(ms_inputTickler,'Marker','o','Color','r')
plot(ms_total,'Marker','x','Color','r')
title('MS + Input tickler')

