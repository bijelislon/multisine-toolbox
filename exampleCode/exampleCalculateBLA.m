
clear variables
close all
clc

%% GENERAL SETTINGS

fs = 4e9; % sampling frequency
P = 3.2; % Number of periods

%% GENERATE ALL SETTINGS.

settingsInst = DataSettings();
settingsInst.FrequencySettings.fs = fs;
settingsInst.NumberOfPeriods = P;
settingsInst.NumberOfInputs = 2;
settingsInst.NumberOfExperiments = 2;

%% GENERATE ALL SIGNALS.

msConfigInst = MultisineConfig(settingsInst);
msConfigInst.GridMode = 'FULL';
msConfigInst.VoltageSettings.bin_voltage = {-40, -40};
msConfigInst.DC = {5, 5};

ms_reference = msConfigInst.generateMultisine();

%% EXPORT DATA & EMULATE SYSTEM.

profile on
exportedData_reference = ms_reference.exportData;

% Emulate a very boring system.
G = [2+1j 6; 3+6j 1; 1.25-0.1j 5];
ms_input = 1.*ms_reference;
ms_input = ms_input.expandPeriods;
ms_input = ms_input.addNoise(1e-6);

ms_output = G*ms_input;
ms_output = ms_output.expandPeriods;
ms_output = ms_output.addNoise(1e-6);
profile off
profile viewer

%% CREATE THE MULTISINE RESPONSE.

LTI_responseInst = LTI_Response(settingsInst,'ref',exportedData_reference,'out',ms_output,'in',ms_input);
LTI_responseInst = LTI_responseInst.calculateBLA('method','ROBUST');

%% PLOT THE CALCULATED BLA

plot(LTI_responseInst.BLA, 'Format', 'r');
hold on
plot(LTI_responseInst.BLA, 'Format', 'i', 'Color', 'r');


