Class Diagram
=============================================

The classes contained inherit and depend on each other in the following ways:

.. image:: img/classDiagram.svg
   :align: center